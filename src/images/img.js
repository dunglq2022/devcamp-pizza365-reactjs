import sile1 from '../images/1.jpg'
import sile2 from '../images/2.jpg'
import sile3 from '../images/3.jpg'
import sile4 from '../images/4.jpg'
import bacon from '../images/bacon.jpg'
import saladBar from '../images/feature-saladbar.jpg'
import hawai from '../images/hawaiian.jpg'
import seaFood from '../images/seafood.jpg'

const image = {
    sile1, sile2, sile3, sile4, bacon, saladBar, hawai, seaFood
}

export {image}