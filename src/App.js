import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import HeaderComponent from './Components/Header';
import FooterComponent from "./Components/Footer"
import ContentComponent from './Components/Content';

function App() {
  return (
    <div>
      <HeaderComponent/>
      <ContentComponent/>
      <FooterComponent/>
    </div>
  );
}

export default App;
