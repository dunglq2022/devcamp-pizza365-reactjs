import { Component } from "react";

class MenuComponent extends Component {
    render() {
        return (
            <nav className="nav nav-pills flex-column flex-sm-row bg-warning fixed-top" >
                <a className="flex-sm-fill text-sm-center nav-link text-body-secondary fs-4" href="http://localhost:3000/">Trang Chủ</a>
                <a className="flex-sm-fill text-sm-center nav-link text-body-secondary fs-4" href="http://localhost:3000/">Combo</a>
                <a className="flex-sm-fill text-sm-center nav-link text-body-secondary fs-4" href="http://localhost:3000/">Loại Pizza</a>
                <a className="flex-sm-fill text-sm-center nav-link text-body-secondary fs-4" href="http://localhost:3000/">Gửi đơn hàng</a>
            </nav>
        )
    }    
}
export default MenuComponent;