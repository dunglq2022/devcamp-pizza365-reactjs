import { Component } from "react";

class SelectSizePizzaComponent extends Component {
    render() {
        return (
            <div>
                <div className="text-center">
                    <h3 className="text-warning" 
                    style={{paddingTop: 40, fontWeight: "bold"}} >Chọn size Pizza</h3>
                    
                    <p className="text-warning" style={{paddingBottom: 20}}>Chọn combo pizza phù hợp với nhu cầu của bạn</p>
                </div>
                
                <div className="col-sm-12">
                    <div className="row">
                    <div className="col-sm-4">
                        <div className="card">
                        <div className="card-header text-black text-center" style={{backgroundColor: "orange"}}>
                            <h3>S (small)</h3>
                        </div>
                        <div className="card-body text-center">
                            <ul className="list-group list-group-flush">
                            <li className="list-group-item">Đường kính: <b>20cm</b></li>
                            <li className="list-group-item">Sườn nướng: <b>2</b></li>
                            <li className="list-group-item">Salad: <b>200g</b></li>
                            <li className="list-group-item">Nước ngọt: <b>2</b></li>
                            <li className="list-group-item">
                                <h1><b>150.000</b></h1>
                                VND
                            </li>
                            </ul>
                        </div>
                        <div className="card-footer text-center d-grid gap-2">
                            <button id="btn-size-small" className="btn btn-warning"
                            style={{fontWeight: "bold"}}
                            data-is-selected-menu="N">Chọn</button>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="card">
                        <div className="card-header bg-warning text-black text-center">
                            <h3>M (medium)</h3>
                        </div>
                        <div className="card-body text-center">
                            <ul className="list-group list-group-flush">
                            <li className="list-group-item">Đường kính: <b>25cm</b></li>
                            <li className="list-group-item">Sườn nướng: <b>4</b></li>
                            <li className="list-group-item">Salad: <b>300g</b></li>
                            <li className="list-group-item">Nước ngọt: <b>3</b></li>
                            <li className="list-group-item">
                                <h1><b>200.000</b></h1>
                                VND
                            </li>
                            </ul>
                        </div>
                        <div className="card-footer text-center d-grid gap-2">
                            <button id="btn-size-medium" className="btn btn-warning"
                            style={{fontWeight: "bold"}}
                            data-is-selected-menu="N">Chọn</button>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="card">
                        <div className="card-header text-black text-center" style={{backgroundColor: "orange"}}>
                            <h3>L (large)</h3>
                        </div>
                        <div className="card-body text-center">
                            <ul className="list-group list-group-flush">
                            <li className="list-group-item">Đường kính: <b>30cm</b></li>
                            <li className="list-group-item">Sườn nướng: <b>8</b></li>
                            <li className="list-group-item">Salad: <b>500g</b></li>
                            <li className="list-group-item">Nước ngọt: <b>4</b></li>
                            <li className="list-group-item">
                                <h1><b>250.000</b></h1>
                                VND
                            </li>
                            </ul>
                        </div>
                        <div className="card-footer text-center d-grid gap-2">
                            <button id="btn-size-large" className="btn btn-warning" 
                            style={{fontWeight: "bold"}}
                            data-is-selected-menu="N">Chọn</button>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default SelectSizePizzaComponent;