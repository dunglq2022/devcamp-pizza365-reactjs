import { Component } from 'react';
import style from '../App.module.css'

class IntroComponent extends Component {
    render() {
        return (
            <div>
                <div>
                    <h3 className="text-warning text-center" style= {{paddingTop: 40, fontWeight: "bold"}}>Tại sao lại Pizza 365</h3>
                    
                </div>
                <div className="row" style={{height: 235}}>
                    <div className="col-sm-3 border border-warning " style={{backgroundColor: 'lightgoldenrodyellow'}}>
                        <div>
                            <h3 className={style.blockGioiThieu}>Đa dạng</h3>
                            <p className="text-details">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay</p>
                        </div>
                    </div>
                    <div className="col-sm-3 border border-warning dinh-dang-block" style={{backgroundColor: "yellow"}}>
                        <h3 className={style.blockGioiThieu}>Chất lượng</h3>
                        <p className="text-details">Nguyện liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                    </div>
                    <div className="col-sm-3 border border-warning dinh-dang-block" style={{backgroundColor: 'lightsalmon'}}>
                        <h3 className={style.blockGioiThieu}>Hương vị</h3>
                        <p className="text-details">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</p>
                    </div>
                    <div className="col-sm-3 border border-warning dinh-dang-block" style={{backgroundColor: "orange"}}>
                        <h3 className={style.blockGioiThieu}>Dịch vụ</h3>
                        <p className="text-details">Nhân viên thân thiện nhà hàng hiện đại.Dịch vụ giao hàng nhanh chất lượng, tân tiến</p>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default IntroComponent;