import { Component } from "react";
import { FaFacebookSquare, FaInstagramSquare, FaLinkedin, FaPinterest, FaSnapchatSquare, FaTelegram } from "react-icons/fa";
class FooterComponent extends Component {
    render() {
        return (
            <nav className="navbar bg-warning">
                <div className="col-sm-12 text-center">
                    <h5>Footer</h5>
                    <button 
                    className="btn btn-dark font-weight-normal"
                    style={
                        {marginTop: '10px',
                        marginBottom: '10px'
                        }
                    }
                    ><i className="fa fa-arrow-up"></i>Top to top</button>
                    <div>
                    <FaFacebookSquare/><FaInstagramSquare/><FaSnapchatSquare/><FaPinterest/><FaLinkedin/><FaTelegram/>
                    </div>
                    <p className="fw-bold">Powered by Quốc Dũng</p>
                </div>
            </nav>
            )
    }
    
}

export default FooterComponent