import { Component } from 'react'
import {image} from '../images/img'
class SelectTypePizzaComponent extends Component {
    render() {
        return (
            <div>
                <div className="text-center">
                    <h3 className="text-warning" style={{paddingTop: 40, fontWeight: 'bold'}}>Chọn size Pizza</h3>
                    
                </div>
                <div className="row row-cols-1 row-cols-md-3 g-4">
                    <div className='col'>
                        <div className="card h-100">
                        <img className="card-img-top" src={image.seaFood} alt='seafood'/>
                            <div className="card-body">
                                <h5 className="card-title">OCEAN MANIA</h5>
                                <p className="card-text" style={{fontSize: 15}}>PIZZA HẢI SẢN SỐT MAYONNAISE</p>
                                <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua,Hành Tây.</p>
                                <div className='d-grid gap-2'>
                                    <button id="btn-type-seafood" data-is-selected-pizza ="N" className="btn btn-warning "  style={{fontWeight: "bold"}}>Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className='col'>
                        <div className="card h-100">
                        <img className="card-img-top" src={image.hawai} alt='hawai'/>
                            <div className="card-body">
                                <h5 className="card-title">HAWAIIAN</h5>
                                <p className="card-text" style={{fontSize: 15}}>PIZZA DĂM BÔNG ĐỨA KIỂU HAWAII.</p>
                                <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                                <div className='d-grid gap-2'>
                                    <button id="btn-type-hawai" data-is-selected-pizza ="N" className="btn btn-warning "  style={{fontWeight: "bold"}}>Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div className='col'>
                        <div className="card">
                        <img className="card-img-top" src={image.bacon} alt='bacon'/>
                            <div className="card-body">
                                <h5 className="card-title">CHESSY CHICKEN BACON</h5>
                                <p className="card-text" style={{fontSize: 15}}>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                <p className="card-text">Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.</p>
                                <div className='d-grid gap-2'>
                                    <button id="btn-type-bacon" data-is-selected-pizza ="N" className="btn btn-warning "  style={{fontWeight: "bold"}}>Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
    
                </div>
            </div>
        )
    }
    
}

export default SelectTypePizzaComponent