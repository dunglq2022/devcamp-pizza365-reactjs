import { Component } from "react"

class SelectDrinkComponent extends Component {
    render() {
        return(
            <div>
                <div className="text-center">
                    <h3 className="text-warning" style={{paddingTop: 40, fontWeight: "bold"}}>Chọn Nước Uống</h3>
                    
                </div>
                <div>
                    <select id="id-select-drinks" className="form-control">
                        <option value="0">Tất cả các nước uống</option>
                        <option value='1'>Cocacola</option>
                    </select>
                </div>
            </div>
        )
    }
    
}

export default SelectDrinkComponent