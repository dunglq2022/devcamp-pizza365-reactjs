import { Component } from "react"
import CaroselComponent from "./Carosel"
import IntroComponent from "./Intro"
import SelectDrinkComponent from "./SelectDrink"
import SelectSizePizzaComponent from "./SelectPizza"
import SelectTypePizzaComponent from "./SelectTypePizza"
import SendOrderComponent from "./SendOrder"

class ContentComponent extends Component {
  render() {
    return (
      <div className="container" style={{marginTop: 80, marginBottom: 50}}>
      <h2 className="text-warning fw-bold">
        PIZZA 365!
      </h2>
      <p style={{fontStyle: 'italic'}} className="text-warning">
          Truly Italian!
      </p>
      <CaroselComponent/>
      <IntroComponent/>
      <SelectSizePizzaComponent/>
      <SelectTypePizzaComponent/>
      <SelectDrinkComponent/>
      <SendOrderComponent/>
    </div>
  )
  }
    
}

export default ContentComponent