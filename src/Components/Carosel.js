import { Component } from 'react'
import {image} from '../images/img' 
import {Carousel} from'react-bootstrap'
class CaroselComponent extends Component {
    render() {
        return (
            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={image.sile1}
                    alt="First slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={image.sile2}
                    alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={image.sile3}
                    alt="Third slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={image.sile4}
                    alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>
        )
    }
    
}

export default CaroselComponent