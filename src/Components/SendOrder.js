import { Component } from "react"

class SendOrderComponent extends Component {
    render() {
        return (
            <div>
                <div className="text-center">
                    <h3 className="text-warning" style={{paddingTop: 40, fontWeight: 'bold'}}>Gửi Đơn Hàng</h3>
                    
                </div>
                <div className="form-group">
                    <div>
                        <label className="form-label">Họ và Tên</label>
                        <input id="inp-fullname" className="form-control" type="text" placeholder="Nhập tên"/>
                    </div>
                    <div>
                        <label className="form-label">Email</label>
                        <input id="inp-email" className="form-control" type="text" placeholder="Nhập email"/>
                    </div>
                    <div>
                        <label className="form-label">Số điện thoại</label>
                        <input id="inp-dien-thoai" className="form-control" type="text" placeholder="Nhập số điện thoại"/>
                    </div>
                    <div>
                        <label className="form-label">Địa chỉ</label>
                        <input id="inp-dia-chi" className="form-control" type="text" placeholder="Nhập địa chỉ"/>
                    </div>
                    <div>
                        <label className="form-label">Mã giảm giá</label>
                        <input id="inp-id-voucher" className="form-control" type="text" placeholder="Nhập mã giảm giá"/>
                    </div>
                    <div>
                        <label className="form-label">Lời nhắn</label>
                        <input id="inp-message" className="form-control" type="text" placeholder="Nhập lời nhắn"/>
                    </div>
                    
                    <div className="d-grid gap-2 pt-3">
                        <button  style={{fontWeight: "bold"}} id="btn-send-order" className="btn btn-warning">Gửi đơn hàng</button>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default SendOrderComponent